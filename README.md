Ember Octane 3
==============

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Ember Octane 3](#ember-octane-3)
    - [Dependencies](#dependencies)
    - [Usage](#usage)
    - [Getting Started](#getting-started)
        - [1. Install Ember](#1-install-ember)
        - [2. Create a New Application](#2-create-a-new-application)
            - [2.1. Write some HTML in a template](#21-write-some-html-in-a-template)
        - [3. Define a Route](#3-define-a-route)
        - [4. Create a UI Component](#4-create-a-ui-component)
            - [4.1. Responding to user interaction](#41-responding-to-user-interaction)
        - [5. Building for Production](#5-building-for-production)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

Dependencies
------------

* [npm](https://npmjs.com)

-------------------------------------------------------------------------------

Usage
-----

``` shell
$ \
npm i   # install dependencies
ember s # run development server
```

The visit:

- <http://localhost:4200>
- <http://localhost:4200/scientists>

-------------------------------------------------------------------------------

Getting Started
---------------

### 1. Install Ember ###

``` shell
$ npm install -g ember-cli
```

### 2. Create a New Application ###

``` shell
$ \
ember new myapp # create app
cd myapp        # go to app
ember s         # serve app
                # M-m m f r s
```

Then visit <http://localhost:4200>.

#### 2.1. Write some HTML in a template ####

``` handlebars
{{! app/templates/application.hbs}}
{{! M-m m f f t}}
<h1>PeopleTracker</h1>

{{outlet}}
```

### 3. Define a Route ###

1. Generate route:

   ``` shell
   $ ember g route scientists
   # M-m m f g r
   ```

2. Update template:

   ``` handlebars
   {{! app/templates/scientists.hbs
   {{! M-m m f f t}}
   <h2>List of Scientists</h2>

   {{outlet}}
   ```

   Then visit <http://localhost:4200/scientists>.

3. Update route:

   ``` javascript
   // app/routes/scientists.js
   // M-m m f f r
   import Route from "@ember/routing/route";

   export default class ScientistsRoute extends Route {
     model() {
       return ["Marie Curie", "Mae Jemison", "Albert Hofmann"];
     }
   }
   ```

4. Re-update template:

   ``` handlebars
   {{! app/templates/scientists.hbs}}
   {{! M-m m f f t}}
   <h2>List of Scientists</h2>

   <ul>
     {{#each @model as |scientist|}}
       <li>{{scientist}}</li>
     {{/each}}
   </ul>

   {{outlet}}
   ```

   Then re-visit <http://localhost:4200/scientists>.

### 4. Create a UI Component ###

1. Create a component:

   ``` shell
   $ ember g component people-list
   # M-m m f g p
   ```

2. Update component:

   ``` handlebars
   {{! app/components/people-list.hbs}}
   {{! M-m m f f p}}
   <h2>{{@title}}</h2>

   <ul>
     {{#each @people as |person|}}
       <li>{{person}}</li>
     {{/each}}
   </ul>

   {{yield}}
   ```

3. Update template:

   ``` handlebars
   {{! app/templates/scientists.hbs }}
   {{! M-m m f f t}}
   <PeopleList @title="List of Scientists" @people={{@model}} />

   {{outlet}}
   ```

#### 4.1. Responding to user interaction ####

1. Update component:

   ``` handlebars
   {{! app/components/people-list.hbs}}
   {{! M-m m f f p}}
   <h2>{{@title}}</h2>

   <ul>
     {{#each @people as |person|}}
       <li>
         <button>{{person}}</button>
       </li>
     {{/each}}
   </ul>

   {{yield}}
   ```

2. Update component script:

   ``` javascript
   // app/components/people-list.js
   // M-m m f f p
   import Component from "@glimmer/component";
   import { action } from "@ember/object";

   export default class PeopleListComponent extends Component {
     @action
     showPerson(person) {
       alert(`The person's name is ${person}!`);
     }
   }
   ```

3. Re-update component:

   ``` handlebars
   {{! app/templates/scientists.hbs}}
   {{! M-m m f f p}}
   <h2>{{@title}}</h2>

   <ul>
     {{#each @people as |person|}}
       <li>
         <button {{on 'click' (fn this.showPerson person)}}>{{person}}</button>
       </li>
     {{/each}}
   </ul>

   {{yield}}
   ```

### 5. Building for Production ###

``` shell
$ ember b --environment=production
# M-m m f r b
```
